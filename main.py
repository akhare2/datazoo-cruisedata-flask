import pymysql
from app import app
from config import mysql
from flask import jsonify
from flask import flash, request
import datetime

@app.route('/AllCruises')
def AllCruises():
    try:
        conn = mysql.connect()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute("SELECT Cruise, BeginYear, BeginMonth, Midpoint FROM Cruise")
        res = cursor.fetchall()
        response = jsonify(res)
        response.status_code = 200
        return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()

@app.route('/CruiseByYear/<int:yr1>')
def cruise_by_year(yr1):
    try:
        conn = mysql.connect()
        yr2 = datetime.datetime.today().strftime("%Y")
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute("SELECT Cruise, BeginYear, BeginMonth, Midpoint FROM Cruise WHERE BeginYear BETWEEN {} and {}".format(yr1, int(yr2)))
        cRow = cursor.fetchall()
        response = jsonify(cRow)
        response.status_code = 200
        return response

    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()

@app.route('/CruiseById/<int:cid>')
def CruiseById(cid):
    try: 
        conn = mysql.connect()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute("SELECT Cruise, BeginYear, BeginMonth, Midpoint FROM Cruise WHERE CruiseID=%s", cid)
        cRow = cursor.fetchone()
        response = jsonify(cRow)
        response.status_code = 200
        return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()

@app.errorhandler(404)
def showMessage(error=None):
    message = {
        'status': 404,
        'message': 'Record not found: ' + request.url,
    }
    response = jsonify(message)
    response.status_code = 404
    return response

if __name__ == "__main__":
    app.run(host="0.0.0.0")
